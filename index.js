console.log('Hello World');

let firstName = `John`;
console.log(`First name: ${firstName}`)

let lastName = `Smith`;
console.log(`Last name: ${lastName}`)

let age = 30;
console.log(`Age: ${age}`)

let Hobbies = [`Biking`, `Mountain Climbing`, `Swimming`];

let address = {
	houseNumber: `32`,
	street: `Washington`,
	city: `Lincoln`,
	state: `Nebraska`
}


function printUserInfo(fn, ln, age, hb, add){
	// console.log(fn)
	// console.log(ln)
	// console.log(age)

	console.log(`${fn} ${ln} is ${age} years of age.`);

	console.log(`This was printed inside of the function`)
	console.log(hb);

	console.log(`This was printed inside of the function`)
	console.log(add);
}

printUserInfo(firstName, lastName, age, Hobbies, address);


function returnFunction(){
	return true
}

let isMarried = returnFunction()
console.log(`The value of isMarried is: ${isMarried}`)